<?php
namespace Mpwarfw\Component;

class Bootstrap
{
    public function __construct()
    {
        echo "<h1>Hola, soy Bootstrap</h1>";
    }

    public function execute()
    {
    	$routing = new Routing();
    	$controller_class = $routing->getRoute();

    	$controller = new $controller_class();
    	$controller->build();
    }

}